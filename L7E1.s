N = 7 
.data
c3: .word 4
c2: .word 2
c1: .word -5
c0: .word 3
.text
.globl main
.ent main
main: subu $sp, $sp, 4 #salvataggio indirizzo di ritorno
sw $ra, ($sp)
lw $t0, c3
lw $t1, c2
lw $t2, c1
lw $t3, c0

#inizializzazioni
li $s0, 8
li $s1, 4
li $s2, 27
li $s3, 9
li $s4, 64
li $s5, 16
add $a0, $t0, $t1
add $a0, $a0, $t2
add $a0, $a0, $t3
mul $a1, $t0, $s0
mul $t8, $t1, $s1
add $a1, $a1, $t8
mul $t8, $t2, 2
add $a1, $a1, $t8
add $a1, $a1, $t3
mul $a2, $t0, $s2
mul $t8, $t1, $s3
add $a2, $a2, $t8
mul $t8, $t2, 3
add $a2, $a2, $t8
add $a2, $a2, $t3
mul $a3, $t0, $s4
mul $t8, $t1, $s5
add $a3, $a3, $t8
mul $t8, $t2, 4
add $a3, $a3, $t8
add $a3, $a3, $t3
#salvataggio registri $tx
subu $sp, $sp, 16
sw $t0, 12($sp)
sw $t1, 8($sp)
sw $t2, 4($sp)
sw $t3, 0($sp)

#passaggio parametro 5
li $t8, N
subu $sp, $sp, 4
sw $t8, ($sp)
jal polinomio
#pop parametro (a vuoto)
addi $sp, $sp, 4
#ripristino registri $tx
lw $t3, 0($sp)
lw $t2, 4($sp)
lw $t1, 8($sp)
lw $t0, 12($sp)
addiu $sp, $sp, 16
#ripristino indirizzo ritorno
lw $ra, ($sp)
addiu $sp, $sp, 4
jr $ra
.end main

.ent polinomio
polinomio: subu $fp, $sp, 4 # usare $fp permette di avere un riferimento
# costante ai parametri ricevuti dal main
#salvataggio registri $sx
subu $sp, $sp, 12
sw $s0, 8($sp)
sw $s1, 4($sp)
sw $s2, 0($sp) # situazione descritta
# dallo schema a fianco ==>
sub $t0, $a1, $a0
sub $t1, $a2, $a1
sub $t2, $a3, $a2
sub $s0, $t1, $t0
sub $s1, $t2, $t1
sub $s2, $s1, $s0
move $v0, $a3
# prelevamento dati da stack
lw $t8, 4($fp)
addi $t8, -4
ciclo: add $s1, $s1, $s2
add $t2, $t2, $s1
add $v0, $v0, $t2
addi $t8, -1
bnez $t8, ciclo
# ripristino registri $sx
lw $s2, 0($sp)
lw $s1, 4($sp)
lw $s0, 8($sp)
addiu $sp, $sp, 12
jr $ra
.end polinomio
