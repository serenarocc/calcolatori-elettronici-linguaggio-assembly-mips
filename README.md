# Calcolatori Elettronici -linguaggio assembly MIPS/ Computer Architecture

La cartella Calcolatori Elettronici- linguaggio assembly MIPS contiene lo svolgimento dei laboratori  del corso di Calcolatori Elettronici del Politecnico di Torino dell'anno 2021.

Il linguaggio utilizzato è MIPS32.

Il simulatore di programmi per MIPS32 utilizzato è QtSpim.

Il codice è introdotto con l'editor notepad++ e salvato in un file con estensione .s. Il file può essere salvato anche con l'estensione .a o .asm

-------------------------------------------------------------
The Computer Architecture directory contains some projects carried out during the course of Computer Architecture at the Politecnico di Torino in 2021.

Languages used is MIPS32.

Ide used is QtSpim.

The code is introduced with the notepad++ editor and saved in a file with the extension .s. The file can also be saved with the extension .a or .asm



## Link ad altri miei progetti personali / Links to other personal projects

- [ ] [Tecniche di Programmazione/ Programming Techniques](https://gitlab.com/serenarocc/tecniche-di-programmazione-linguaggio-c)
- [ ] [Algoritmi linguaggio C / Algorithms](https://gitlab.com/serenarocc/algoritmi-linguaggio-c)
- [ ] [Algoritmi e Strutture Dati / Algorithms and Data Structures](https://gitlab.com/serenarocc/algoritmi-e-strutture-dati-linguaggio-c)
- [ ] [Sistemi Operativi/ Operating Systems](https://gitlab.com/serenarocc/sistemi-operativi) 
- [ ] [Calcolatori Elettronici/Computer architecture](https://gitlab.com/serenarocc/calcolatori-elettronici-linguaggio-assembly-mips)
- [ ] [Basi di Dati /Databases](https://gitlab.com/serenarocc/basi-di-dati)
- [ ] [Programmazione a oggetti /Object Oriented Programming](https://gitlab.com/serenarocc/object-oriented-programming-java)
