DIM = 3
DIM_RIGA = DIM * 4
.data
mat1: .word 4, -45, 15565, 6458, 4531, 124, -548, 2124, 31000
mat2: .word 6, -5421, -547, -99, 4531, 1456, 4592, 118, 31999
indice: .word 2
vet_out: .space DIM_RIGA
.text
.globl main
main: subu $sp, $sp, 4
sw $ra, ($sp)
la $a0, mat1
la $a1, mat2
la $a2, vet_out
li $a3, DIM
subu $sp, $sp, 4
lw $t0, indice
sw $t0, ($sp)
jal Variazione
addu $sp, $sp, 4
lw $ra, ($sp)
addu $sp, $sp, 4
jr $ra

.ent Variazione
Variazione:
subu $sp, $sp, 4 # si lavora nell'ipotesi di non avere overflow
sw $ra, ($sp)
move $fp, $sp
sll $t8, $a3, 2
lw $t0, 4($fp)
mul $t1, $t0, $t8
addu $a0, $a0, $t1 # indirizzo primo elemento della prima matrice
mul $t1, $t0, 4
addu $a1, $a1, $t1 # indirizzo primo elemento della seconda matrice 
li $t1, 0 # contatore
ciclo1: lw $t2, ($a0)
lw $t3, ($a1)
subu $sp, $sp, 8
sw $a0, ($sp)
sw $a1, 4($sp)
move $a0, $t2 # ELEMENTO RIGA - VAL1
move $a1, $t3 # ELEMENTO COLONNA - VAL2 
jal CalcoloVariazione # a titolo di esempio si usa una seconda procedura per il calcolo
# ma non e’ strettamente necessario
lw $a0, ($sp)
lw $a1, 4($sp)
addiu $sp, 8
sw $v0, ($a2)
addiu $a0, $a0, 4
addiu $a1, $a1, $t8
addiu $a2, $a2, 4
addiu $t1, $t1, 1
bne $t1, $a3, ciclo1 
lw $ra, ($sp)
addu $sp, $sp, 4
jr $ra # return
.end Variazione
.ent CalcoloVariazione
CalcoloVariazione: 
sub $t0, $a1, $a0
mul $t0, $t0, 100
div $v0, $t0, $a0
jr $ra # return
.end CalcoloVariazione
